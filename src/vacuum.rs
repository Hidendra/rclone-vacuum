use crate::Opt;
use std::collections::HashSet;
use std::fs;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::process::{Command, Stdio};
use std::time::Instant;

struct ServiceAccounts {
    /// Directory where service account configs are stored
    config_dir: String,

    /// Account files that have been discovered
    discovered_accounts: HashSet<String>,

    /// Service accounts that are online and working
    online: Vec<String>,

    /// Service accounts that have errored and are offline
    /// They will be ignored for a period of time before being retried.
    /// The 'period of time' is purely best-effort.
    /// Checking if a mount works is to simply online it and see.
    offline: Vec<String>,
    // The last index used for round-robin fetching
    // TODO this is disabled to allow testing bw limits more predictably
    // without cycling accounts
    //    last_rr_index: usize,
}

impl ServiceAccounts {
    fn new(config_dir: String) -> Self {
        Self {
            config_dir,
            discovered_accounts: HashSet::new(),
            online: Vec::new(),
            offline: Vec::new(),
            //            last_rr_index: 0,
        }
    }

    /// Gets a path to the next service account to use to transfer files.
    /// It will generally return accounts in a round-robin fashion.
    fn next(&mut self) -> Option<String> {
        if self.online.is_empty() {
            None
        } else {
            //            let index = self.last_rr_index;
            //            self.last_rr_index += 1;
            //
            //            Some(self.online.get(index % self.online.len()).unwrap().clone())
            Some(self.online.first().unwrap().clone())
        }
    }

    /// Sets a mount to be offline
    fn set_offline(&mut self, path: &str) {
        if let Some(position) = self.online.iter().position(|p| p == path) {
            debug!("Disabling service account: {}", path);
            self.offline.push(self.online.remove(position));
        }
    }

    /// Sets a mount to be online
    //    fn set_online(&mut self, path: &str) {
    //        if let Some(position) = self.offline.iter().position(|p| p == path) {
    //            self.online.push(self.offline.remove(position));
    //        }
    //    }

    /// Looks in the config directory for new accounts
    /// They will be added to the online set if any new accounts are fine
    fn discover_new_accounts(&mut self) {
        for entry in fs::read_dir(&self.config_dir).expect("Could not read config_dir") {
            if let Ok(entry) = entry {
                let path = entry.path().to_str().unwrap().to_string();

                if path.ends_with(".json") && !self.discovered_accounts.contains(&path) {
                    info!("Found new service account config: {}", &path);
                    self.discovered_accounts.insert(path.clone());
                    self.online.push(path);
                }
            }
        }
    }

    /// Checks for offline accounts and moves them to the online list.
    /// This is currently pretty dumb.
    fn check_offline_accounts(&mut self) {
        if !self.offline.is_empty() {
            self.online.append(&mut self.offline);
            debug!("Enabled all disabled service accounts");
        }
    }
}

pub struct Vacuum {
    source_dirs: Vec<String>,
    rclone_target: String,
    service_accounts: ServiceAccounts,
    rclone_opts: Vec<String>,
}

impl Vacuum {
    pub fn new(opts: &Opt) -> Self {
        Self {
            source_dirs: opts.source_dirs.clone(),
            rclone_target: opts.rclone_target.clone(),
            service_accounts: ServiceAccounts::new(opts.service_account_config_dir.clone()),
            rclone_opts: opts.rclone_opts.clone(),
        }
    }

    /// Ticks the vacuum and transfers any pending files
    pub fn tick(&mut self) {
        self.service_accounts.discover_new_accounts();

        // If any accounts failed to transfer files, no accounts will be re-onlined
        // to simulate a dumb best-effort account enablement.
        // If an account FAILS, that means a different one should be tried, so none should be
        // re-onlined. That way, others will be tried first... We have no way of saying
        // "don't try this account for 10 minutes" yet since this is kept simple...
        let mut any_failed = false;

        for source_dir in &self.source_dirs {
            if let Some(service_account) = self.service_accounts.next() {
                if !self.try_rclone_move(source_dir, service_account.clone()) {
                    self.service_accounts.set_offline(&service_account);
                    any_failed = true;

                    break;
                }
            } else {
                break;
            }
        }

        if !any_failed {
            self.service_accounts.check_offline_accounts();
        }
    }

    /// Tries to execute an rclone move from the given directory with the given account
    fn try_rclone_move(&self, source_dir: &str, service_account_config_path: String) -> bool {
        // NB: This has already been verified to be a directory on startup.
        let source_dir_path = Path::new(source_dir);

        // nothing to transfer in empty dirs
        if source_dir_path.read_dir().map(|mut i| i.next().is_none()).unwrap_or(false) {
            return true;
        }

        let dir_name = source_dir_path
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .to_string();
        let rclone_dest = format!("{}:/{}", &self.rclone_target, dir_name);

        let mut args = vec![
            "move".to_string(),
            source_dir.to_string(),
            rclone_dest,
            "--drive-service-account-file".to_string(),
            service_account_config_path,
            "--log-level".to_string(),
            "DEBUG".to_string(),
            "--retries".to_string(),
            "1".to_string(), // never retry
        ]
        .to_vec();

        args.extend(self.rclone_opts.iter().cloned());

        debug!("Running rclone with args: {:?}", args.join(" "));

        let start = Instant::now();

        let mut command = Command::new("rclone")
            .args(&args)
            .stderr(Stdio::piped())
            .spawn()
            .unwrap();

        {
            let stderr = command.stderr.as_mut().unwrap();
            let reader = BufReader::new(stderr);

            for line in reader.lines() {
                if let Ok(line) = line {
                    debug!("[rclone] {}", line);
                }
            }
        }

        let status = command.wait().unwrap();

        // TODO output time it took
        let elapsed = start.elapsed();
        debug!("Rclone took {:?}; status={:?}", elapsed, status);

        status.success()
    }
}
