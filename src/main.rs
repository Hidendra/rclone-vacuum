#[macro_use]
extern crate log;

use env_logger::Env;
use std::time::Duration;
use structopt::StructOpt;

mod vacuum;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "rclone-vacuum",
    about = "Multi-account uploading to Google Shared Drives"
)]
pub struct Opt {
    /// The directories that will be transferred to remote
    /// The final directory name will be MOVED to the rclone destination under the final directory's
    /// name.
    /// Example: source=/data/vacuum/TV; dest=gsuite-crypt
    ///          All files/dirs inside /data/vacuum/TV will be transferred to gsuite-crypt:TV
    #[structopt(short, long)]
    source_dirs: Vec<String>,

    /// The number of seconds between rclone runs
    #[structopt(long, default_value = "600")]
    sleep_interval_secs: u64,

    /// The destination rclone target.
    /// All source directories will be transferred into this rclone target.
    /// This does NOT need to be configured w/ an account necessarily. The important thing
    /// is that the `team_drive` setting is set and any crypt settings are configured.
    /// The `service_account_file` option will be automatically set based on the config
    /// chosen.
    #[structopt(short, long)]
    rclone_target: String,

    /// The directory that contains service account config files. They must have .json extension.
    #[structopt(long)]
    service_account_config_dir: String,

    /// Arguments to pass to rclone directly.
    /// This must be '--' followed by the rclone args, e.g. `-- --transfers 4 --min-age 1m`
    rclone_opts: Vec<String>,
}

fn main() {
    let env = Env::default().filter_or("RUST_LOG", "debug");
    env_logger::init_from_env(env);

    let opts = Opt::from_args();

    if opts.source_dirs.is_empty() {
        error!("Source dirs cannot be empty");
        return;
    }

    for source_dir in &opts.source_dirs {
        if !std::fs::metadata(source_dir)
            .expect("Could not find source_dir")
            .is_dir()
        {
            error!("Source directory {} is not a valid directory", source_dir);
            return;
        }
    }

    if !std::fs::metadata(&opts.service_account_config_dir)
        .expect("Could not find service account dir")
        .is_dir()
    {
        error!(
            "service_account_config_dir: {} is not a valid directory",
            opts.service_account_config_dir
        );
        return;
    }

    let mut vacuum = vacuum::Vacuum::new(&opts);

    loop {
        vacuum.tick();

        debug!("sleeping");
        std::thread::sleep(Duration::from_secs(opts.sleep_interval_secs));
    }
}
